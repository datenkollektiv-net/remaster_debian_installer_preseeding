# Scripts to generate custom Debian installer with preseeding config

This script does easy remastering of debian installer (amd64 only at this time) with a custom preseed.cfg.

It's playing together with https://gitlab.com/datenkollektiv-net/debpull and preconfiguration
of dknansiblepulleployment.

It:
* generates ssh-keys for ansible-pull deployment,
* generates user password for default user,
* checks if packages exists in choosen debian release.

## Requirements
* ansible
* xorriso
* bsdtar

## Steps to genearate iso image

`cp preseed_vars.yml.example to preseed_vars.yml`
`cp vars.example to vars`

Adjust preseed_vars.yml to your needs:

`vim preseed_vars`

Configure variables in vars:

`vim vars`

Run remastering script:

`./remaster_mini-iso_deb-installer.sh`

## Result:

You get an netinstall iso with custom preseeding configuration.

You shoud evaluate the generated `preseed.cfg`.
