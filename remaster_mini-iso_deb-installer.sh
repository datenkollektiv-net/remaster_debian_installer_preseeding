#!/bin/bash

# INSTALL_DIR=""
DESTINATION=isofiles
INSTALL_DIR=install.amd
PRESEEDING_CONF=$(pwd)/preseed.cfg
ORIG_ISO_NAME=debian-netinstall-orig.iso

source vars

if ! which ansible-playbook > /dev/null; then
    echo "Ansible is an requirement"
    exit 0
fi

if ! test -f preseed_vars.yml; then
    echo "No preseed_vars.yml found"
    exit 0
fi

# generate git deploy key if needed
if grep "^dknansiblepulldeploy:" preseed_vars.yml | grep -i true; then
    if ! test -f $GIT_DEPLOY_KEY; then
        read -p "Shall we generate a new git deploy-key for ansiblepull-deployment? [y/n]" -r GEN_GIT_KEY
        if [ "$GEN_GIT_KEY" == "y" ]; then
            ssh-keygen -t ed25519 -f $GIT_DEPLOY_KEY
        fi
    fi
fi

# convert git diploy key
if test -f $GIT_DEPLOY_KEY; then
    GIT_DEPLOY_KEY_ONLINE=$(tr -d "\n" < $GIT_DEPLOY_KEY)
    sed -i 's|^dknansiblepulldeploy__deploy_key:.*|dknansiblepulldeploy__deploy_key: '"$GIT_DEPLOY_KEY_ONLINE"'|' preseed_vars.yml
fi

# check for user credentials in preseed_vars.yml
if [ "$(grep passwd__user_password_crypted preseed_vars.yml | wc -c)" -lt 100 ]; then
    # ask for password
    echo "It seems, there is no password for the default user configured."
    read -p "Do you want to insert password now? [Y|n]" -r INSERT_PW

    if [ "$INSERT_PW" != "n" ]; then
        read -p "Password: " -r SYSTEM_USER_PW
        SYSTEM_USER_PW_CRYPT=$(openssl passwd -6 "$SYSTEM_USER_PW")
    fi

    sed -i 's|^passwd__user_password_crypted:.*|passwd__user_password_crypted: '"$SYSTEM_USER_PW_CRYPT"'|' preseed_vars.yml
else
    echo "Password already set."
fi

# generate preseed.cfg from jinja template
ansible-playbook mkpreseed.yml || exit 0

# remastering image
if  test -e "$ORIG_ISO_NAME"; then
   echo "Debian-Image already exists"
   echo "Download Image again? [y/N]"
   read -r download_image

   if [ "$download_image" == "y" ]; then
      wget $INSTALLER_URL -O "$ORIG_ISO_NAME" || exit 0
   fi

else
      wget $INSTALLER_URL -O "$ORIG_ISO_NAME" || exit 0
fi

if ! test -e "$PRESEEDING_CONF"; then
    echo "no preseeding config found."
    exit 0
fi

mkdir ${DESTINATION}

# unpack iso
bsdtar -C ${DESTINATION}/ -xf "$ORIG_ISO_NAME"

cd ${DESTINATION} || exit
mkdir initrd-tmp
cd initrd-tmp || exit

# unpack initrd
zcat ../$INSTALL_DIR/initrd.gz | cpio -iv

# add preseeding config to initrd root dir
cp "$PRESEEDING_CONF" preseed.cfg

# make original initrd writable
chmod +w ../$INSTALL_DIR/initrd.gz

# create new initrd
find . -print0 | cpio -0 -H newc -ov | gzip -c > ../$INSTALL_DIR/initrd.gz

cd ..
rm -r initrd-tmp
cd ..

# regenearte md5sum.txt
cd ${DESTINATION} || exit
chmod +w md5sum.txt
find . -follow -type f ! -name md5sum.txt -print0 | xargs -0 md5sum > md5sum.txt
chmod -w md5sum.txt
cd ..

# make writable
chmod -R +w ${DESTINATION}

# create iso according https://wiki.debian.org/RepackBootableISO"
mbr_template=isohdpfx.bin

dd if="$ORIG_ISO_NAME" bs=1 count=432 of="$mbr_template"

xorriso -as mkisofs \
   -r -V "$ISO_LABEL" \
   -o "${INSTALLER_NAME}_$(date +%Y-%m-%d).iso" \
   -J -J -joliet-long -cache-inodes \
   -isohybrid-mbr "$mbr_template" \
   -b isolinux/isolinux.bin \
   -c isolinux/boot.cat \
   -boot-load-size 4 -boot-info-table -no-emul-boot \
   -eltorito-alt-boot \
   -e boot/grub/efi.img \
   -no-emul-boot -isohybrid-gpt-basdat -isohybrid-apm-hfsplus \
   "$DESTINATION"

# cleanup
chmod -R +w ${DESTINATION}
rm -r ${DESTINATION}
rm $mbr_template

echo ""
echo "*********************************************"
echo "We are ready."
echo "*********************************************"

read -p "Shall we keep the original installer image? [y/N]" -r DELETE_ISO
if [ "$DELETE_ISO" == "y" ]; then
        echo "The original installer image is kept."
    else
        rm $ORIG_ISO_NAME
        echo "The original installer image was deleted."
fi

exit 0
